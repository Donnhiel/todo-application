<?php

    if(isset($_POST["submit"])){

        $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_SPECIAL_CHARS);

        if(!empty($name)){

            if(preg_match('/^[A-Za-z\s\-]+$/', $name)){
                echo "Hello, {$name}! Welcome to our website.";
            }else{
                echo "<script>
                    alert('Your name is invalid!');
                </script>";
            }

        }else{
            echo "<script>
                    alert('Please input you name!');
                </script>";
        }

    }
      

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INDEX | GETTING USER INPUT</title>
</head>
<body>

    <form method="POST">
        <label for="name">Enter your name:</label>
        <input type="text" id="name" name="name">
        <button name="submit" type="submit">Submit</button>
    </form>

</body>
</html>
