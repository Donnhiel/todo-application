<?php
$conn = new mysqli("localhost", "root", "", "backend");

    if(!$conn){
        echo "error";
    }

    //Insert data into the employee table
    $sql = "INSERT INTO `employee` (first_name, last_name, middle_name, birthday, address) VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street'); ";
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    //retrieve the first name, last name, and birthday of all employees in the table
    $sql = "SELECT first_name, last_name, birthday FROM employee";
    $result = $conn->query($sql);


    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    
    //retrieve the number of employees whose last name starts with the letter 'S'
    $sql =  "SELECT COUNT(*) AS counter FROM employee WHERE last_name LIKE '%Jane%'";
    $result = $conn->query($sql);


    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }

    //retrieve the first name, last name, and address of the employee with the highest ID number
    $sql =  "SELECT first_name, last_name, address FROM employees WHERE id = (SELECT MAX(id) FROM employees)";
    $result = $conn->query($sql);


    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
        }
    } else {
        echo "0 results";
    }

    //Update data in the employee table  
    $newAddress = '123 Main Street';

    $sql =  "UPDATE employee SET address = '$newAddress' WHERE first_name = 'John'";
    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }

    //Delete data from the employee table
    $sql = "DELETE FROM employee WHERE last_name LIKE 'D%'";
    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
    $conn->close();

?>