<?php

    class API {
        public function userName($fullName) {
            echo "Fullname: ".$fullName."<br><br>";
        }
        
        public function userHobby($hobbies) {
            echo "Hobbies:<br>";
            foreach ($hobbies as $hobby) {
                echo "<ul>
                        <li>$hobby</li>
                    </ul>";
            }
        }
        
        public function others($userData) {
            echo "Age: " . $userData->age . "<br>";
            echo "Email: " . $userData->email . "<br>";
            echo "Birthday: " . $userData->birthday . "<br>";
        }
    }

    $api = new API();

    $api->userName("Juan Dela Cruz");

    $hobbies = array("Playing Basketball", "Reading Books", "Listening Music");
    $api->userHobby($hobbies); 

    $userData = (object) array(
        'age' => 20,
        'email' => 'juandelacruz@emailniya.com',
        'birthday' => 'January 1, 1987'
    );
    $api->others($userData);

?>
