<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APISuccessTest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testPayload()
    {
        $payload = array(
            'first_name' => 'John',
            'middle_name' => 'Smith',
            'last_name' => 'Doe',
            'contact_number' => 654655
        );

        $this->assertNotEmpty($payload);

        return $payload;
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $payload = $this->testPayload();
        $result = json_decode($this->api->httpPost($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('success', $result['status']);
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);

        return $result['data'];
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpGet($postData)
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array(
            'id' => $postData['id']
        );
        $result = json_decode($this->api->httpGet($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('success', $result['status']);
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);

        return $result['data'];
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpPut($postData)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $payload = array(
            'id' => $postData['id'],
            'first_name' => 'UpdatedName',
            'middle_name' => 'UpdatedMiddle',
            'last_name' => 'UpdatedLast',
            'contact_number' => '039645456465asd'
        );

        $result = json_decode($this->api->httpPut($postData['id'], $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('success', $result['status']);
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);
    }

    /**
     * @depends testHttpGet
     */
    public function testHttpDelete($postData)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $payload = array(
            'id' => $postData['id']
        );

        $result = json_decode($this->api->httpDelete('information', $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('success', $result['status']);
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);
    }
}
