<?php
/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");

/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");

/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');

class API {
    private $db;
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }
/**
 * HTTP GET Request
 *
 * @param $payload
 */
public function httpGet($payload)
{
    if (is_array($payload) && isset($payload['id'])) {
        $id = $payload['id'];
        $result = $this->db->where('id', $id)->getOne('information');
        if ($result) {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $result
            ));
        } else {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'message' => "Employee with ID={$id} does not exist."
            ));
        }
    } elseif (is_numeric($payload)) {
        $id = $payload;
        $result = $this->db->where('id', $id)->getOne('information');
        if ($result) {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $result
            ));
        } else {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'message' => "Employee with ID={$id} does not exist."
            ));
        }
    } else {
        $result = $this->db->get('information');
        if ($result) {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $result
            ));
        } else {
            return json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'message' => 'Failed to retrieve data.'
            ));
        }
    }
}

/**
 * HTTP POST Request
 * @param $payload
 */
public function httpPost($payload)
{
    if (is_array($payload) && !empty($payload)) {
        // Validate fields
        $errors = $this->Validation($payload);
        if (!empty($errors)) {
            return json_encode(array(
                'method' => 'POST',
                'status' => 'fail',
                'message' => 'Invalid field values',
                'data' => null, // Include the 'data' key with a null value
                'errors' => $errors
            ));
        }

        $insertId = $this->db->insert('information', $payload);
        if ($insertId) {
            // Retrieve the inserted record using the ID
            $insertedData = $this->db->where('id', $insertId)->getOne('information');

            if ($insertedData) {
                // Add the ID to the retrieved record
                $insertedData['id'] = $insertId;

                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'success',
                    'data' => $insertedData,
                ));
            } else {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'fail',
                    'message' => 'Failed to retrieve inserted data',
                ));
            }
        } else {
            return json_encode(array(
                'method' => 'POST',
                'status' => 'fail',
                'message' => 'Failed to Insert Data',
            ));
        }
    } else {
        return json_encode(array(
            'method' => 'POST',
            'status' => 'fail',
            'message' => 'Invalid or empty payload. Payload must be a non-empty array.'
        ));
    }
}

/**
 * Validate the fields in the payload.
 * Returns an array of field validation errors.
 *
 * @param array $payload
 * @return array
 */
private function Validation($payload)
{
    $errors = array();

    // Validate first_name
    if (isset($payload['first_name']) && !preg_match('/^[a-zA-Z]+$/', $payload['first_name'])) {
        $errors['first_name'] = 'First name must contain only letters.';
    }

    // Validate middle_name
    if (isset($payload['middle_name']) && !preg_match('/^[a-zA-Z]+$/', $payload['middle_name'])) {
        $errors['middle_name'] = 'Middle name must contain only letters.';
    }

    // Validate last_name
    if (isset($payload['last_name']) && !preg_match('/^[a-zA-Z]+$/', $payload['last_name'])) {
        $errors['last_name'] = 'Last name must contain only letters.';
    }

    return $errors;
}

/**
 * HTTP PUt Request
 *
 * @param $payload
 */
public function httpPut($id, $payload)
{
    // Check if the provided ID is a number
    if (!is_numeric($id)) {
        return json_encode(array(
            'method' => 'PUT',
            'status' => 'fail',
            'message' => "Invalid ID. ID must be a number."
        ));
    }
    
    if (!empty($payload)) {
        $checkID = $this->db->where('id', $id)->getOne('information');
        if ($checkID) {
            // Validate fields
            $errors = $this->Validation($payload);
            if (!empty($errors)) {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'fail',
                    'message' => $errors
                ));
            }

            $this->db->where('id', $id);
            $result = $this->db->update('information', $payload);
            if ($result) {
                $updatedData = $this->db->where('id', $id)->getOne('information');
                $updatedData['id'] = $id;

                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'success',
                    'data' => $updatedData
                ));
            } else {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'fail',
                    'message' => 'Failed to update data.'
                ));
            }
        } else {
            return json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'message' => "Employee with ID={$id} does not exist."
            ));
        }
    } else {
        return json_encode(array(
            'method' => 'PUT',
            'status' => 'fail',
            'message' => 'Empty payload.'
        ));
    }
}

/**
 * HTTP DELETE Request
 *
 * @param $id
 * @param $payload
 */
public function httpDelete($id, $payload)
{

    if (empty($payload)) {
        return json_encode(array(
            'method' => 'DELETE',
            'status' => 'fail',
            'message' => 'Empty payload.'
        ));
    }

    if (empty($payload['id'])) {
        return json_encode(array(
            'method' => 'DELETE',
            'status' => 'fail',
            'message' => 'Please input ID in the payload.'
        ));
    }
    // Check if the provided ID is a number
    if (!is_numeric($id=$payload['id'])) {
        return json_encode(array(
            'method' => 'DELETE',
            'status' => 'fail',
            'message' => "Invalid ID. ID must be a number."
        ));
    }
    $id = $payload['id'];
    $checkID = $this->db->where('id', $id)->getOne('information');
    if (!$checkID) {
        return json_encode(array(
            'method' => 'DELETE',
            'status' => 'fail',
            'message' => "Employee with ID={$id} does not exist."
        ));
    }

    if (!is_array($payload)) {
        $this->db->where('id', $id);
        $result = $this->db->delete('information');

        if ($result) {
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'data' => array($checkID) // Include the deleted data in the response
            ));
        } else {
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'message' => 'Failed to delete data.'
            ));
        }
    } else {
        $this->db->where('id', $payload, 'IN');
        $result = $this->db->delete('information');

        if ($result) {
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'success',
                'data' => array($checkID) // Include the deleted data in the response
            ));
        } else {
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'message' => 'Failed to delete data.'
            ));
        }
    }
} 

}

//Identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];

// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];


        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));


        $last_index = count($exploded_request_uri) - 1;


        $ids = $exploded_request_uri[$last_index];


        }
    }
    //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);

    $api = new API;

    //Checking if what type of request and designating to specific functions
     switch ($request_method) {
         case 'GET':
            $response = $api->httpGet($received_data);
             break;
         case 'POST':
            $response = $api->httpPost($received_data);
             break;
         case 'PUT':
            $response = $api->httpPut($ids, $received_data);
             break;
         case 'DELETE':
            $response = $api->httpDelete($ids, $received_data);
             break;
     }

    if (isset($response)) {
        echo $response;
    }

?>