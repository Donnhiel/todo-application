<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APIFailedTest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testPayload()
    {
        $payload = array(
            'first_name' => 123, // Invalid value
            'middle_name' => 456, // Invalid value
            'last_name' => 789, // Invalid value
            'contact_number' => 'asd'
        );

        $this->assertNotEmpty($payload);

        return $payload;
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $payload = $this->testPayload();
        $response = $this->api->httpPost($payload);
        $result = json_decode($response, true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('fail', $result['status']);
        $this->assertArrayHasKey('errors', $result);

        $result['data']['id'] = 123;

        return $result['data'];
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpGet($postData)
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array(
            'id' => $postData['id']
        );
        $result = json_decode($this->api->httpGet($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('fail', $result['status']);
        $this->assertArrayHasKey('message', $result);
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpPut($postData)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $payload = array(
            'id' => $postData['id'],
            'first_name' => 1242342,
            'middle_name' => 142424,
            'last_name' => 3435647,
            'contact_number' => 'abcdefghi'
        );

        $result = json_decode($this->api->httpPut($postData['id'], $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('fail', $result['status']);
        $this->assertArrayHasKey('message', $result);
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpDelete($postData)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $payload = array(
            'id' => $postData['id']
        );
        $result = json_decode($this->api->httpDelete('information', $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals('fail', $result['status']);
        $this->assertArrayHasKey('message', $result);
    }
}
