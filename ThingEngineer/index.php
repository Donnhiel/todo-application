<?php
require ('MysqliDb.php');
$conn = new mysqliDb("localhost", "root", "", "backend");

/*################## INSERTING DATA IN DATABASE ####################*/
if(isset($_POST["add"])){

    $firstname = filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_SPECIAL_CHARS);
    $lastname = filter_input(INPUT_POST, "lastname", FILTER_SANITIZE_SPECIAL_CHARS);
    $middlename = filter_input(INPUT_POST, "middlename", FILTER_SANITIZE_SPECIAL_CHARS);
    $birthday = filter_input(INPUT_POST, "birthday", FILTER_SANITIZE_NUMBER_INT);
    $address = filter_input(INPUT_POST, "address", FILTER_SANITIZE_SPECIAL_CHARS);

    if(preg_match('/^[A-Za-z\s\-]+$/', $firstname) && preg_match('/^[A-Za-z\s\-]+$/', $lastname) && preg_match('/^[A-Za-z\s\-]+$/', $middlename)){
        
        if(preg_match('/^[a-zA-Z0-9\s]+$/', $address)) {
            
            $data = Array("first_name" => $firstname,
                          "last_name" => $lastname,
                          "middle_name" => $middlename,
                          "birthday" =>  $birthday,
                          "address" => $address);
            $add = $conn->insert ('employee', $data);
            if($add){
            echo "<script>
                    alert('User added successfully!');
                </script>";
            }

        }else {
            echo "<script>
                    alert('The address you provide is invalid!');
                </script>";
        }

    }else{
        echo "<script>
                 alert('Your name is invalid!');
             </script>";
    }

}

/*################## UPDATING DATA IN DATABASE ####################*/
if(isset($_POST["update"])) {
    $userID = $_POST['id'];
    $updateFirstname = filter_input(INPUT_POST, "updateFirstname", FILTER_SANITIZE_SPECIAL_CHARS);
    $updateLastname = filter_input(INPUT_POST, "updateLastname", FILTER_SANITIZE_SPECIAL_CHARS);
    $updateMiddlename = filter_input(INPUT_POST, "updateMiddlename", FILTER_SANITIZE_SPECIAL_CHARS);
    $updateBirthday = filter_input(INPUT_POST, "updateBirthday", FILTER_SANITIZE_NUMBER_INT);
    $updateAddress = filter_input(INPUT_POST, "updateAddress", FILTER_SANITIZE_SPECIAL_CHARS);

    if(preg_match('/^[A-Za-z\s\-]+$/', $updateFirstname) && preg_match('/^[A-Za-z\s\-]+$/', $updateLastname) && preg_match('/^[A-Za-z\s\-]+$/', $updateMiddlename)){
        if(preg_match('/^[a-zA-Z0-9\s]+$/', $updateAddress)) {
            $data = Array(
                "first_name" => $updateFirstname,
                "last_name" => $updateLastname,
                "middle_name" => $updateMiddlename,
                "birthday" =>  $updateBirthday,
                "address" => $updateAddress);
            $conn->where("id", $userID);
            $update = $conn->update('employee', $data);
            if($update){
                echo "<script>alert('User updated successfully!');</script>";
            }
        } else {
            echo "<script>alert('The address you provided is invalid!');</script>";
        }
    } else {
        echo "<script>alert('Your name is invalid!');</script>";
    }
}

/*################## DELETING DATA IN DATABASE ####################*/
if(isset($_GET['deleteID'])) {
    $userID = $_GET['deleteID'];
    $conn->where("id", $userID);
    $delete = $conn->delete('employee');
    if($delete){
        echo "<script>alert('User deleted successfully!');</script>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD | ThingEngineer</title>
</head>
<style>
    div{
        display: flex;
        justify-content: space-around;
        align-items: center;
    }
</style>
<body>
    <div>
        <form action="index.php" method="post" autocomplete="off">
            <h1>ADD USER</h1>
            <input type="text" name="firstname" placeholder="Enter your firstname" required><br>
            <input type="text" name="lastname" placeholder="Enter your lastname" required><br>
            <input type="text" name="middlename" placeholder="Enter your middlename" required><br>
            <input type="date" max="9999-12-31" name="birthday" required><br>
            <input type="text" name="address" placeholder="Enter your address" required><br>
            <input type="submit" name="add" value="Add">
        </form>
        <?php
            if (isset($_GET['updateID'])) {
                $userID = $_GET['updateID'];
                $data = Array("first_name", "middle_name", "last_name", "birthday", "address");
                $conn->where("id", $userID);
                $users = $conn->get("employee", null, $data);
                if ($conn->count > 0) {
                    foreach ($users as $row) {
                        ?>
                        <form action='index.php' method='POST'>
                            <h1>UPDATE USER</h1>
                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                            <input type="text" name="updateFirstname" value="<?php echo $row['first_name']; ?>"><br>
                            <input type="text" name="updateLastname" value="<?php echo $row['middle_name']; ?>"><br>
                            <input type="text" name="updateMiddlename" value="<?php echo $row['last_name']; ?>"><br>
                            <input type="date" name="updateBirthday" value="<?php echo $row['birthday']; ?>"><br>
                            <input type="text" name="updateAddress" value="<?php echo $row['address']; ?>"><br>
                            <input type="submit" value="Update" name="update">
                        </form>
                        <?php
                    }
                }
            }
        ?>
    </div>
    <center>
        <h1>EMPLOYEE LIST</h1>
        <table border="1">
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Middlename</th>
            <th>Address</th>
            <th>Birthdate</th>
            <th colspan="2">Action</th>
        </tr>
        <?php
/* ############################# DISPLAYING FETCHED DATA IN BROWSER ############################### */
            $employees = $conn->get('employee');
            
            foreach($employees as $employee) {
                echo "<tr>";
                    echo "<td>".$employee['first_name']."</td>";
                    echo "<td>".$employee['last_name']."</td>";
                    echo "<td>".$employee['middle_name']."</td>";
                    echo "<td>".$employee['address']."</td>";
                    echo "<td>".$employee['birthday']."</td>";
                    echo "<td><a href='index.php?updateID=".$employee['id']."'>Update</a></td>";
                    echo "<td><a href='index.php?deleteID=".$employee['id']."' onclick='return confirm(\"Are you sure you want to delete this user?\")'>Delete</a></td>";
                echo "</tr>";
            }
            ?>
        </table>
    </center>
</body>
</html>
