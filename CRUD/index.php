<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EXERCISE 7 | BASIC CRUD</title>
    <style>
        *{
            padding: 0;
            margin: 0;
            font-family: sans-serif;
        }
        #title{
            text-align: center;
        }
        table{
            display: flex;
            justify-content: space-evenly;
            align-items: center;
        }
        td{
            border: 1px solid rgb(1, 26, 247);
            text-align: center;
            padding: 10px;
        }
        th{
            border: 1px dashed red;
            padding: 15px;

        }
        a{
            text-decoration: none;
        }
        div{
            display: flex;
            justify-content: space-evenly;
            align-items: center;
            width: 100%;
            height: 40vh;
            text-align: center;
        }
        #addbtn{
            width: 100px;
            padding: 10px;
            color: #fff;
            background-color: rgb(35, 107, 241);
            outline: none;
            border: none;
            margin-top: 5px;
        }
        #updatebtn{
            width: 100px;
            padding: 10px;
            color: #fff;
            background-color: rgb(22, 160, 33);
            outline: none;
            border: none;
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <div>
        <!--################# FORM FOR ADDING USER ####################--> 
        <form action="index.php" method="post" autocomplete="off">
            <h1>ADD USER</h1>
            <input type="text" name="firstname" placeholder="Enter your firstname" required><br>
            <input type="text" name="lastname" placeholder="Enter your lastname" required><br>
            <input type="text" name="middlename" placeholder="Enter your middlename" required><br>
            <input type="date" max="9999-12-31" name="birthday" required><br>
            <input type="text" name="address" placeholder="Enter your address" required><br>
            <input type="submit" id="addbtn" name="add" value="Add">
        </form>
        <!--################# FORM FOR UPDATING USER ####################--> 
        <form action="index.php" method="post" autocomplete="off">
            <?php
                $conn = new mysqli("localhost", "root", "", "backend");
                // Retrieve the user ID from the query parameter
                $userID = isset($_GET["userID"]) ? $_GET["userID"] : null;

                if ($userID !== null) {
                    // Fetch the user data from the database based on the user ID
                    $sql = "SELECT * FROM employee WHERE id=?";
                    $stmt = $conn->prepare($sql);
                    $stmt->bind_param("i", $userID);
                    $stmt->execute();
                    $result = $stmt->get_result();
                    $row = $result->fetch_assoc();
            ?>
            <h1>UPDATE USER</h1>
            <input type="hidden" name="userID" value="<?php echo $userID; ?>">
            <input type="text" name="firstname" value="<?php echo $row['first_name']; ?>" required><br>
            <input type="text" name="lastname" value="<?php echo $row['last_name']; ?>" required><br>
            <input type="text" name="middlename" value="<?php echo $row['middle_name']; ?>" required><br>
            <input type="date" max="9999-12-31" name="birthday" value="<?php echo $row['birthday']; ?>" required><br>
            <input type="text" name="address" value="<?php echo $row['address']; ?>" required><br>
            <input type="submit" id="updatebtn" name="update" value="Update">
            <?php
                }
            ?>
        </form>
    </div>
</body>
</html>
<?php
$conn = new mysqli("localhost", "root", "", "backend");
/*################## INSERTING DATA IN DATABASE ####################*/
    if(isset($_POST["add"])){

        $firstname = filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_SPECIAL_CHARS);
        $lastname = filter_input(INPUT_POST, "lastname", FILTER_SANITIZE_SPECIAL_CHARS);
        $middlename = filter_input(INPUT_POST, "middlename", FILTER_SANITIZE_SPECIAL_CHARS);
        $birthday = filter_input(INPUT_POST, "birthday", FILTER_SANITIZE_NUMBER_INT);
        $address = filter_input(INPUT_POST, "address", FILTER_SANITIZE_SPECIAL_CHARS);

        if(preg_match('/^[A-Za-z\s\-]+$/', $firstname) && preg_match('/^[A-Za-z\s\-]+$/', $lastname) && preg_match('/^[A-Za-z\s\-]+$/', $middlename)){
            
            if(preg_match('/^[a-zA-Z0-9\s]+$/', $address)) {
                
                $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES (?, ?, ?, ?, ?);";

                $stmt = $conn->prepare($sql);
                $stmt->bind_param("sssss", $firstname, $lastname, $middlename, $birthday, $address);

                if ($stmt->execute()) {
                    echo "<script>
                            alert('User added successfully!');
                        </script>";
                } else {
                    echo "Error";
                }

            }else {
                echo "<script>
                        alert('The address you provide is invalid!');
                    </script>";
            }

        }else{
            echo "<script>
                     alert('Your name is invalid!');
                 </script>";
        }

    }

/*################## DISPLAYING DATA IN BROWSER ####################*/
    // Retrieve data from the "employee" table
    $sql = "SELECT * FROM employee";
    $result = $conn->query($sql);

    // Display the data in the browser
    echo "<h1 id='title'>Employee List</h1>";
    echo "<table>";
    echo "<tr><th>First Name</th><th>Last Name</th><th>Middle Name</th><th>Birthday</th><th>Address</th><th colspan='2'>Action</th></tr>";

    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>".$row['first_name']."</td>";
        echo "<td>".$row['last_name']."</td>";
        echo "<td>".$row['middle_name']."</td>";
        echo "<td>".$row['birthday']."</td>";
        echo "<td>".$row['address']."</td>";
        echo "<td><a href='index.php?userID=".$row['id']."'>Update</a></td>";
        echo "<td><a href='delete.php?userID=".$row['id']."' onclick='return confirm(\"Are you sure you want to delete this user?\")'>Delete</a></td>";
        echo "</tr>";
    }

    echo "</table>";

    /*################## UPDATING DATA IN BROWSER ####################*/
    if (isset($_POST["update"])) {
        // Retrieve the updated values from the form fields
        $userID = filter_input(INPUT_POST, "userID", FILTER_SANITIZE_NUMBER_INT);
        $firstname = filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_SPECIAL_CHARS);
        $lastname = filter_input(INPUT_POST, "lastname", FILTER_SANITIZE_SPECIAL_CHARS);
        $middlename = filter_input(INPUT_POST, "middlename", FILTER_SANITIZE_SPECIAL_CHARS);
        $birthday = filter_input(INPUT_POST, "birthday", FILTER_SANITIZE_NUMBER_INT);
        $address = filter_input(INPUT_POST, "address", FILTER_SANITIZE_SPECIAL_CHARS);

        if (preg_match('/^[A-Za-z\s\-]+$/', $firstname) && preg_match('/^[A-Za-z\s\-]+$/', $lastname) && preg_match('/^[A-Za-z\s\-]+$/', $middlename)) {
            if (preg_match('/^[a-zA-Z0-9\s]+$/', $address)) {
                // Update the user data in the database
                $sql = "UPDATE employee SET first_name=?, last_name=?, middle_name=?, birthday=?, address=? WHERE id=?;";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param("sssssi", $firstname, $lastname, $middlename, $birthday, $address, $userID);
                if ($stmt->execute()) {
                    echo "<script>
                            alert('User updated successfully!');
                            window.location.href = 'index.php';
                        </script>";
                    exit;
                } else {
                    echo "Error: " . $stmt->error;
                }
            } else {
                echo "<script>
                        alert('Invalid address!');
                    </script>";
            }
        } else {
            echo "<script>
                    alert('Invalid name!');
                </script>";
        }
    }

    // Close the database connection
    $conn->close();

?>
