<?php
$conn = new mysqli("localhost", "root", "", "backend");

if (isset($_GET["userID"])) {
    $userID = $_GET["userID"];

    // Delete the user from the database
    $sql = "DELETE FROM employee WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $userID);

    if ($stmt->execute()) {
        echo "<script>
                alert('User deleted successfully!');
                window.location.href = 'index.php';
            </script>";
        exit;
    } else {
        echo "Error deleting user: " . $conn->error;
    }
}

// Close the database connection
$conn->close();
?>
